(ns plf01.core)

(defn función-<-1
  [a b]
  (< a b))

(defn función-<-2
  [a b c]
  (< a b c))

(defn función-<-3
  [a b c d]
  (< a b c d))

;Ejemplos:
(función-<-1 1 2)
(función-<-2 6 5 4)
(función-<-3 6 5 4 3)

(defn función-<=-1
  [a b]
  (<= a b))

(defn función-<=-2
  [a b c]
  (<= a b c))

(defn función-<=-3
  [a b c d]
  (<= a b c d))

;Ejemplos:
(función-<=-1 1 2)
(función-<=-2 6 5 4)
(función-<=-3 6 5 4 3)

(defn función-==-1
  [a b]
  (== a b))

(defn función-==-2
  [a b c]
  (== a b c))

(defn función-==-3
  [a b c d]
  (== a b c d))

;Ejemplos:
(función-==-1 1 2)
(función-==-2 6 6 6)
(función-==-3 6 5 4 3)

(defn función->-1
  [a b]
  (> a b))

(defn función->-2
  [a b c]
  (> a b c))

(defn función->-3
  [a b c d]
  (> a b c d))

;Ejemplos:
(función->-1 1 2)
(función->-2 6 5 4)
(función->-3 6 5 4 3)

(defn función->=-1
  [a b]
  (>= a b))

(defn función->=-2
  [a b c]
  (>= a b c))

(defn función->=-3
  [a b c d]
  (>= a b c d))

;Ejemplos:
(función->=-1 1 2)
(función->=-2 6 5 4)
(función->=-3 6 5 4 3)

(defn función-assoc-1
  [a b c]
  (assoc a b c))

(defn función-assoc-2
  [a b c]
  (assoc a b c))

(defn función-assoc-3
  [a b c]
  (assoc a b c))

;Ejemplos:
(función-assoc-1 {} :key1 "value")
(función-assoc-2 [1 2 3] 0 10)
(función-assoc-3 nil :key1 4)

(defn función-assoc-in-1
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-2
  [a b c]
  (assoc-in a b c))

(defn función-assoc-in-3
  [a b c]
  (assoc-in a b c))

;Ejemplos:
(función-assoc-in-1  {} [1 :connections 4] 2)
(función-assoc-in-2 {} [:cookie :monster :vocals] "Finntroll")
(función-assoc-in-3 {:person {:name "Mike"}} [:person :name] "Violet")


(defn función-concat-1
  [a b]
  (concat a b))

(defn función-concat-2
  [a b c]
  (concat a b c))

(defn función-concat-3
  [a b c d]
  (concat a b c d))

;Ejemplos:
(función-concat-1 [1 2] [3 4])
(función-concat-2 [:a :b] nil [1 [2 3] 4])
(función-concat-3 [2] '(3 4) [5 6 7] #{9 10 8})


(defn función-conj-1
  [a b]
  (conj a b))

(defn función-conj-2
  [a b c]
  (conj a b c))

(defn función-conj-3
  [a b c d]
  (conj a b c d))

;Ejemplos:
(función-conj-1 ["a" "b" "c"] "d")
(función-conj-2 '(1 2) 3 4)
(función-conj-3 [1 2] 3 4 5)


(defn función-cons-1
  [a b]
  (cons a b))

(defn función-cons-2
  [a b]
  (cons a b))

(defn función-cons-3
  [a b]
  (cons a b))

;Ejemplos:
(función-cons-1 1 '(2 3 4 5 6))
(función-cons-2 [1 2] [4 5 6])
(función-cons-3 {1 2} {4 5 6 7})


(defn función-contains?-1
  [a b]
  (contains? a b))

(defn función-contains?-2
  [a b]
  (contains? a b))

(defn función-contains?-3
  [a b]
  (contains? a b))

;Ejemplos:
(función-contains?-1 {:a 1} :a)
(función-contains?-2 {:a nil} :a)
(función-contains?-3 {:a 1} :b)

(defn función-count-1
  [a]
  (count a))

(defn función-count-2
  [a]
  (count a))

(defn función-count-3
  [a]
  (count a))

;Ejemplos:
(función-count-1 nil)
(función-count-2 [1 2 3])
(función-count-3 {:one 1 :two 2})


(defn función-disj-1
  [a]
  (disj a))

(defn función-disj-2
  [a b]
  (disj a b))

(defn función-disj-3
  [a b c]
  (disj a b c))

;Ejemplos:
(función-disj-1 #{1 2 3})
(función-disj-2 #{1 2 3} 2)
(función-disj-3 #{1 2 3} 1 3)

(defn función-dissoc-1
  [a]
  (dissoc a))

(defn función-dissoc-2
  [a b]
  (dissoc a b))

(defn función-dissoc-3
  [a b c]
  (dissoc a b c))

;Ejemplos:
(función-dissoc-1 {:a 1 :b 2 :c 3})
(función-dissoc-2 {:a 1 :b 2 :c 3} :b)
(función-dissoc-3 {:a 1 :b 2 :c 3} :c :b)


(defn función-distinct-1
  [a]
  (distinct a))

(defn función-distinct-2
  [a]
  (distinct a))

(defn función-distinct-3
  [a]
  (distinct a))

;Ejemplos:
(función-distinct-1 [1 2 1 3 1 4 1 5])
(función-distinct-2 '(1 2 3 4 5 4 3 2))
(función-distinct-3 nil)


(defn función-distinct?-1
  [a b]
  (distinct? a b))

(defn función-distinct?-2
  [a b c]
  (distinct? a b c))

(defn función-distinct?-3
  [a b c d]
  (distinct? a b c d))

;Ejemplos:
(función-distinct?-1 1 2)
(función-distinct?-2 1 2 3)
(función-distinct?-3 1 2 3 3)

(defn función-drop-last-1
  [a]
  (drop-last a))

(defn función-drop-last-2
  [a b]
  (drop-last a b))

(defn función-drop-last-3
  [a b]
  (drop-last a b))

;Ejemplos:
(función-drop-last-1 [1 2 3 4])
(función-drop-last-2 -1 [1 2 3 4])
(función-drop-last-3 0 [1 2 3 4])

(defn función-empty-1
  [a]
  (empty a))

(defn función-empty-2
  [a]
  (empty a))

(defn función-empty-3
  [a]
  (empty a))

;Ejemplos:
(función-empty-1 '(1 2))
(función-empty-2 [1 2])
(función-empty-3 {1 2})


(defn función-empty?-1
  [a]
  (empty? a))

(defn función-empty?-2
  [a]
  (empty? a))

(defn función-empty?-3
  [a]
  (empty? a))

;Ejemplos:
(función-empty?-1 ())
(función-empty?-2 '(1))
(función-empty?-3 [1 2 3 4])


(defn función-even?-1
  [a]
  (even? a))

(defn función-even?-2
  [a]
  (even? a))

(defn función-even?-3
  [a]
  (even? a))

;Ejemplos:
(función-even?-1 2)
(función-even?-2 1)
(función-even?-3 6)


(defn función-false?-1
  [a]
  (false? a))

(defn función-false?-2
  [a]
  (false? a))

(defn función-false?-3
  [a]
  (false? a))

;Ejemplos:
(función-false?-1 false)
(función-false?-2 true)
(función-false?-3 nil)


(defn función-find-1
  [a b]
  (find a b))

(defn función-find-2
  [a b]
  (find a b))

(defn función-find-3
  [a b]
  (find a b))

;Ejemplos:
(función-find-1 {:a 1 :b 2 :c 3} :a)
(función-find-2 {:a nil} :a)
(función-find-3 {:a 1 :b 2 :c 3} :d)

(defn función-first-1
  [a]
  (first a))

(defn función-first-2
  [a]
  (first a))

(defn función-first-3
  [a]
  (first a))

;Ejemplos:
(función-first-1 [1 2 3])
(función-first-2 {:a 3 :b 4 :c 6})
(función-first-3 #{3 4 5})


(defn función-flatten-1
  [a]
  (flatten a))

(defn función-flatten-2
  [a]
  (flatten a))

(defn función-flatten-3
  [a]
  (flatten a))

;Ejemplos:
(función-flatten-1 [1 [2 3]])
(función-flatten-2 '(1 2 3))
(función-flatten-3 '(1 2 [3 (4 5)]))


(defn función-frequencies-1
  [a]
  (frequencies a))

(defn función-frequencies-2
  [a]
  (frequencies a))

(defn función-frequencies-3
  [a]
  (frequencies a))

;Ejemplos:
(función-frequencies-1 ['a 'b 'a 'a])
(función-frequencies-2 {:a 2 :b 1 :c 3})
(función-frequencies-3 [3 6 2 6 8 7 'b 'c 3 5 3 4 7 6 'a])

(defn función-get-1
  [a b]
  (get a b))

(defn función-get-2
  [a b]
  (get a b))

(defn función-get-3
  [a b]
  (get a b))

;Ejemplos:
(función-get-1 [1 2 3] 1)
(función-get-2 [1 2 3] 5)
(función-get-3 {:a 1 :b 2} :b)

(defn función-get-in-1
  [a b]
  (get-in a b))

(defn función-get-in-2
  [a b]
  (get-in a b))

(defn función-get-in-3
  [a b]
  (get-in a b))

;Ejemplos:
(función-get-in-1 {:a 1 :b 2 :c 3} {:b 9 :d 4})
(función-get-in-2 {:a 1, :b 2} nil)
(función-get-in-3 {:a [{:b1 2} {:b2 4}] :c 3} [:a 0 :b1])

(defn función-into-1
  [a b]
  (into a b))

(defn función-into-2
  [a b]
  (into a b))

(defn función-into-3
  [a b]
  (into a b))

;Ejemplos:
(función-into-1 {:a 1 :b 2 :c 3} {:d 9 :e 4})
(función-into-2 () '(1 2 3))
(función-into-3 [1 2 3] '(4 5 6))

(defn función-key-1
  [a]
  (key a))

(defn función-key-2
  [a]
  (key a))

(defn función-key-3
  [a]
  (key a))

;Ejemplos:
(map función-key-1 {:a 1 :b 2})
(función-key-2 (clojure.lang.MapEntry. :a :b))
(map función-key-3 {:a 1 :b 2 :c 3 :d 4})

(defn función-keys-1
  [a]
  (keys a))

(defn función-keys-2
  [a]
  (keys a))

(defn función-keys-3
  [a]
  (keys a))

;Ejemplos:
(función-keys-1 {:a 1 :b 2 :c 3})
(función-keys-2 {:a 1})
(función-keys-3 nil)

(defn función-max-1
  [a]
  (max a))

(defn función-max-2
  [a b]
  (max a b))

(defn función-max-3
  [a b c]
  (max a b c))

;Ejemplos:
(función-max-1 100)
(función-max-2 10 20)
(función-max-3 10 20 30)

(defn función-merge-1
[a b] 
(merge a b))

(defn función-merge-2
  [a b]
  (merge a b))

(defn función-merge-3
  [a b]
  (merge a b))

;Ejemplos:
(función-merge-1 {:a 1 :b 2 :c 3} {:b 9 :d 4})
(función-merge-2 {:a 1} nil)
(función-merge-3 nil {:a 1})

(defn función-min-1
  [a b]
  (min a b))

(defn función-min-2
  [a b c]
  (min a b c))

(defn función-min-3
  [a b c d]
  (min a b c d))

;Ejemplos:
(función-min-1 2 1)
(función-min-2 3 2 1)
(función-min-3 4 3 2 1)

(defn función-neg?-1
  [a]
  (neg? a))

(defn función-neg?-2
  [a b]
  (neg? (/ a b)))

(defn función-neg?-3
  [a b c]
  (neg? (/ (- a b) c)))

;Ejemplos:
(función-neg?-1 0)
(función-neg?-2 1 2)
(función-neg?-3 4 6 8)

(defn función-nil?-1
  [a]
  (nil? a))

(defn función-nil?-2
  [a]
  (nil? a))

(defn función-nil?-3
  [a]
  (nil? a))

;Ejemplos:
(función-nil?-1 1)
(función-nil?-2 nil)
(función-nil?-3 false)

(defn función-not-empty-1
  [a]
  (not-empty a))

(defn función-not-empty-2
  [a]
  (not-empty a))

(defn función-not-empty-3
  [a]
  (not-empty a))

;Ejemplos:
(función-not-empty-1 [1 2 3])
(función-not-empty-2 nil)
(función-not-empty-3 [ ])

(defn función-nth-1
  [a b]
  (nth a b))

(defn función-nth-2
  [a b]
  (nth a b))

(defn función-nth-3
  [a b c]
  (nth a b c))

;Ejemplos:
(función-nth-1 [1 2 3] 0)
(función-nth-2 [1 2 3] 1)
(función-nth-3 [] 0 "nothing found")

(defn función-odd?-1
   [a]
   (odd? a))

(defn función-odd?-2
  [a]
  (odd? a))

(defn función-odd?-3
  [a]
  (odd? a))

;Ejemplos:
(función-odd?-1 1)
(función-odd?-2 2)
(función-odd?-3 0)

(defn función-partition-1
  [a b]
  (partition a b))

(defn función-partition-2
  [a b]
  (partition a b))

(defn función-partition-3
  [a b c]
  (partition a b c))

;Ejemplos:
(función-partition-1 2 (range 20))
(función-partition-2 5 (range 10))
(función-partition-3 4 6 (range 20))

(defn función-partition-all-1
  [a b]
  (partition-all a b))

(defn función-partition-all-2
  [a b]
  (partition-all a b))

(defn función-partition-all-3
  [a b c]
  (partition-all a b c))

;Ejemplos:
(función-partition-all-1 3 (range 10))
(función-partition-all-2 4 (range 10))
(función-partition-all-3 3 6 (range 20))

(defn función-peek-1
  [a]
  (peek a))

(defn función-peek-2
  [a]
  (peek a))

(defn función-peek-3
  [a]
  (peek a))

;Ejemplos:
(función-peek-1 [1 2 3 4])
(función-peek-2 [])
(función-peek-3 '(1 2 3 4))

(defn función-pop-1
  [a]
  (pop a))

(defn función-pop-2
  [a]
  (pop a))

(defn función-pop-3
  [a]
  (pop a))

;Ejemplos:
(función-pop-1 [1 2 3 4])
(función-pop-2 [1])
(función-pop-3 '(1 2 3 4))

(defn función-pos?-1
  [a]
  (pos? a))

(defn función-pos?-2
  [a]
  (pos? a))

(defn función-pos?-3
  [a b]
  (pos? (/ a b)))

;Ejemplos:
(función-pos?-1 1)
(función-pos?-2 -1)
(función-pos?-3 2 3)

(defn función-quot-1
  [a b]
  (quot a b))

(defn función-quot-2
  [a b]
  (quot a b))

(defn función-quot-3
  [a b]
  (quot a b))

;Ejemplos:
(función-quot-1 10 3)
(función-quot-2 12 3)
(función-quot-3 10 -3)

(defn función-range-1
  [a]
  (range a))

(defn función-range-2
  [a b]
  (range a b))

(defn función-range-3
  [a b c]
  (range a b c))

;Ejemplos:
(función-range-1 10)
(función-range-2 -5 5)
(función-range-3 -100 100 10)

(defn función-rem-1
  [a b]
  (rem a b))

(defn función-rem-2
  [a b]
  (rem a b))

(defn función-rem-3
  [a b]
  (rem a b))

;Ejemplos:
(función-rem-1 10 9)
(función-rem-2 2 2)
(función-rem-3 10 -3)

(defn función-repeat-1
  [a]
  (repeat a))

(defn función-repeat-2
  [a b]
  (repeat a b))

(defn función-repeat-3
  [a b]
  (repeat a b))

;Ejemplos:
(función-repeat-1 "x")
(función-repeat-2 5 "x")
(función-repeat-3 10 "l")

(defn función-replace-1
  [a b]
  (replace a b))

(defn función-replace-2
  [a b]
  (replace a b))

(defn función-replace-3
  [a b]
  (replace a b))

;Ejemplos:
(función-replace-1 [:zeroth :first :second :third :fourth] [0 2 4 0])
(función-replace-2 [10 9 8 7 6] [0 2 4])
(función-replace-3 {2 :a, 4 :b} [1 2 3 4])

(defn función-rest-1
  [a]
  (rest a))

(defn función-rest-2
  [a]
  (rest a))

(defn función-rest-3
  [a]
  (rest a))

;Ejemplos:
(función-rest-1 [1 2 3 4 5])
(función-rest-2 ["a" "b" "c" "d" "e"])
(función-rest-3 nil)

(defn función-select-keys-1
  [a b]
  (select-keys a b))

(defn función-select-keys-2
  [a b]
  (select-keys a b))

(defn función-select-keys-3
  [a b]
  (select-keys a b))

;Ejemplos:
(función-select-keys-1 {:a 1 :b 2} [:a])
(función-select-keys-2 {:a 1 :b 2} [:b :c])
(función-select-keys-3 {:a 1 :b 2 :c 3} [:a :c])

(defn función-shuffle-1
  [a]
  (shuffle a))

(defn función-shuffle-2
  [a]
  (shuffle a))

(defn función-shuffle-3
  [a]
  (shuffle a))

;Ejemplos:
(función-shuffle-1 (list 1 2 3))
(función-shuffle-2 ["a" "b" "c" "d" "e"])
(función-shuffle-3 [1 2 3])

(defn función-sort-1
  [a]
  (sort a))

(defn función-sort-2
  [a]
  (sort a))

(defn función-sort-3
  [a]
  (sort a))

;Ejemplos:
(función-sort-1 [3 1 2 4])
(función-sort-2 ["d" "b" "a" "c" "e"])
(función-sort-3 {:b 1 :c 3 :a  2})

(defn función-split-at-1
  [a b]
  (split-at a b))

(defn función-split-at-2
  [a b]
  (split-at a b))

(defn función-split-at-3
  [a b]
  (split-at a b))

;Ejemplos:
(función-split-at-1 2 [1 2 3 4 5])
(función-split-at-2 3 ["a" "b" "c" "d" "e"])
(función-split-at-3 3 [1 2])

(defn función-str-1
  [a]
  (str a))

(defn función-str-2
  [a b]
  (str a b))

(defn función-str-3
  [a b c]
  (str a b c))

;Ejemplos:
(función-str-1 1)
(función-str-2 1 "hola")
(función-str-3 9 "1" "a")

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b]
  (subs a b))

(defn función-subs-3
  [a b c]
  (subs a b c))

;Ejemplos:
(función-subs-1 "Clojure" 1)
(función-subs-2 "1s2d3r4t" 2)
(función-subs-3 "Clojure" 1 3)

(defn función-subvec-1
  [a b]
  (subvec a b))

(defn función-subvec-2
  [a b]
  (subvec a b))

(defn función-subvec-3
  [a b c]
  (subvec a b c))

;Ejemplos:
(función-subvec-1 [1 2 3 4 5 6 7] 2)
(función-subvec-2 ["a" "b" "c" "d" "e"] 3)
(función-subvec-3 [1 2 3 4 5 6 7] 2 4)

(defn función-take-1
  [a b]
  (take a b))

(defn función-take-2
  [a b]
  (take a b))

(defn función-take-3
  [a b]
  (take a b))

;Ejemplos:
(función-take-1 3 [1 2 3 4 5 6])
(función-take-2 2 ["a" "b" "c" "d" "e"])
(función-take-3 3 '(1 2 3 4 5 6))

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

;Ejemplos:
(función-true?-1 1)
(función-true?-2 true)
(función-true?-3 (= 1 1))

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

;Ejemplos:
(función-val-1 (clojure.lang.MapEntry. "key" "val"))
(función-val-2 (clojure.lang.MapEntry. :a :b))
(función-val-3 (first {:one :two}))

(defn función-vals-1
  [a]
  (vals a))

(defn función-vals-2
  [a]
  (vals a))

(defn función-vals-3
  [a]
  (vals a))

;Ejemplos:
(función-vals-1 {:a "foo", :b "bar"})
(función-vals-2  {})
(función-vals-3 nil)

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  (zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

;Ejemplos:
(función-zero?-1 0)
(función-zero?-2 2r000)
(función-zero?-3 (/ 1 2))

(defn función-zipmap-1
  [a b]
  (zipmap a b))

(defn función-zipmap-2
  [a b]
  (zipmap a b))

(defn función-zipmap-3
  [a b]
  (zipmap a b))

;Ejemplos:
(función-zipmap-1 [:a :b :c :d :e] [1 2 3 4 5])
(función-zipmap-2 [:a :b :c] [1 2 3 4])
(función-zipmap-3 [:a :b :c] [1 2])
